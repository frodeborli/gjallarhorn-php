<?php
namespace Gjallarhorn;

use BackedEnum;
use Closure;

/**
 * An interface defining an event emitting class.
 * 
 * @package Gjallarhorn
 */
interface EventEmitterInterface {
    /**
     * Receive a notification for an event only once
     * 
     * @param int|string|BackedEnum $event 
     * @param Closure $listener 
     * @return void 
     */
    public function once(int|string|BackedEnum $event, Closure ...$listener): void;

    /**
     * Receive all notifications for a particular event
     * 
     * @param int|string|BackedEnum $event 
     * @param Closure ...$listener 
     * @return void 
     */
    public function on(int|string|BackedEnum $event, Closure ...$listener): void;
    
    /**
     * Stop receiving notifications for a particular event
     * 
     * @param int|string|BackedEnum $event 
     * @param Closure ...$handlers 
     * @return int The number of event listeners that were removed
     */
    public function off(int|string|BackedEnum $event, Closure ...$listener): int;
}