<?php

use Gjallarhorn\EventEmitterInterface;
use Gjallarhorn\EventEmitterTrait;

require(getenv('THEMIS_AUTOLOAD_PATH') ?: '../vendor/autoload.php');

Themis::expectEvent('TEST_EVENT');

class EventSource implements EventEmitterInterface {
    use EventEmitterTrait;

    public function testEmit(): void {
        $this->emit('TEST_EVENT', 123);
    }
}

$es = new EventSource();
$es->on('TEST_EVENT', function($value) {
    Themis::logEvent('TEST_EVENT');
});
$es->testEmit();