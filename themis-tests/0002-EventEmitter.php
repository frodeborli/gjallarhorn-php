<?php

use Gjallarhorn\EventEmitter;

require(getenv('THEMIS_AUTOLOAD_PATH') ?: '../vendor/autoload.php');

Themis::expectEvent('SYNC_EVENT','ASYNC_EVENT','SYNC_EVENT');

$es = new EventEmitter();

$es->on('SYNC_EVENT', function() {
    Themis::logEvent('SYNC_EVENT');
});

$es->on('ASYNC_EVENT', function() {
    Themis::logEvent('ASYNC_EVENT');
});

$es->on('ASYNC_EVENT', function() use ($es) {
    $es->emit('SYNC_EVENT');
});

$es->emit('SYNC_EVENT');
$es->emitAsync('ASYNC_EVENT');
