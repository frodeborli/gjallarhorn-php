<?php

use Gjallarhorn\Event;
use Gjallarhorn\EventEmitter;
use Gjallarhorn\EventEmitterInterface;
use Gjallarhorn\EventEmitterTrait;

require(getenv('THEMIS_AUTOLOAD_PATH') ?: '../vendor/autoload.php');

$emitter = new EventEmitter();

$emitter->on("test-event", function($event) {
    Themis::logEvent("test-event");
    assert($event instanceof Event, "Expected an event object");
});

Themis::expectEvent("test-event");

$emitter->emit(new Event("test-event", ["test" => "test"]));
