<?php
require(__DIR__.'/../vendor/autoload.php');
use Gjallarhorn\{
    EventEmitterInterface,
    EventEmitterTrait
};
use Skuld\Promise;

enum MyEvent: string {
    case GOOD_EVENT = 'GOOD_EVENT';
    case BAD_EVENT = 'BAD_EVENT';
}

/**
 * @method void on(MyEvent $event, Closure ...$listener) Subscribe to an event
 * @method void off(MyEvent $event, Closure ...$listener) Unsubscribe from an event
 * @method void once(MyEvent $event, Closure ...$listener) Subscribe only to the next invocation of the event
 */
class SomeClass implements EventEmitterInterface {
    use EventEmitterTrait;

    public function triggerGoodEventAsync(): Promise {
        return $this->emitAsync(MyEvent::GOOD_EVENT, $this);
    }

    public function triggerGoodEvent(): void {
        $this->emit(MyEvent::GOOD_EVENT, $this);
    }

}

$instance = new SomeClass();

$instance->on(MyEvent::GOOD_EVENT, function($value) {
    echo "Listener1: sleep(2)\n";
    Promise::sleep(2)->wait();
    echo "Listener1: Done sleeping\n";
});

$instance->on(MyEvent::GOOD_EVENT, function($value) {
    echo "Listener2: sleep(2)\n";
    Promise::sleep(2)->wait();
    echo "Listener2: Done sleeping\n";
});

$result = $instance->triggerGoodEventAsync();
echo "ALL DONE\n";