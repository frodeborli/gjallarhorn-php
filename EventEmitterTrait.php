<?php
namespace Gjallarhorn;

use BackedEnum;
use Closure;
use LogicException;
use FiberError;
use Skuld\Promise;
use Psr\Log\LoggerInterface;
use Throwable;
use RuntimeException;
use WeakMap;

trait EventEmitterTrait {
    /**
     * For injecting a PSR-3 LoggerInterface instance
     * 
     * @var null|LoggerInterface
     */
    protected ?LoggerInterface $eventEmitterLogger = null;
    private array $eventSubscribers = [];
    private ?WeakMap $onceListeners = null;

    /**
     * Remove all event listeners, optionally restricted to only a particular event
     * 
     * @param int|string|BackedEnum|null $event 
     * @return int 
     */
    protected function clearEventListeners(int|string|BackedEnum $event=null): int {
        if ($event === null) {
            $this->eventSubscribers = [];
            $this->onceListeners = new WeakMap();
        }
        $event = $event instanceof BackedEnum ? $event->value : $event;
        if (empty($this->eventSubscribers[$event])) {
            return 0;
        }
        $count = 0;
        foreach ($this->eventSubscribers[$event] as $listener) {
            ++$count;
            if (isset($this->onceListeners[$listener])) {
                if (0 === --$this->onceListeners[$listener]) {
                    unset($this->onceListeners[$listener]);
                }
            }
        }
        return $count;
    }

    /**
     * Trigger an event asynchronously. This will return a Promise object
     * which can be resolved by calling `$promise->wait()`.
     * 
     * @param int|string|BackedEnum $event 
     * @param array $args 
     * @return Promise 
     * @throws LogicException 
     * @throws FiberError 
     * @throws Throwable 
     * @throws RuntimeException 
     */
    protected function emitAsync(int|string|BackedEnum|Event $event, mixed ...$args): Promise {

        if ($event instanceof Event) {
            $eventName = $event->event;
            if ($eventName instanceof BackedEnum) {
                $eventName = $event->value;
            }
        } else {
            $eventName = $event instanceof BackedEnum ? $event->value : $event;
        }

        if (empty($this->eventSubscribers[$eventName])) {
            $this->eventEmitterLogger?->debug("No subscribers for event", ['event' => $event]);
            return Promise::fulfilled([]);
        }
        $promises = [];
        foreach ($this->eventSubscribers[$eventName] as $subscriber) {
            $promises[] = new Promise(function() use ($subscriber, $event, $args) {
                try {
                    return $subscriber($event, ...$args);
                } catch (Throwable $e) {
                    $this->eventEmitterLogger?->error($e);
                    throw $e;
                }
            });
        }

        return Promise::all(...$promises);
    }

    /**
     * Trigger an event synchronously and wait for all listeners to respond.
     * 
     * @param int|string|BackedEnum $event 
     * @param array $args 
     * @return mixed 
     * @throws LogicException 
     * @throws FiberError 
     * @throws Throwable 
     * @throws RuntimeException 
     */
    protected function emit(int|string|BackedEnum|Event $event, mixed ...$args): mixed {
        return $this->emitAsync($event, ...$args)->wait();
    }

    /**
     * @inheritdoc
     */
    public function once(int|string|BackedEnum $event, Closure ...$listeners): void {
        $event = $event instanceof BackedEnum ? $event->value : $event;
        foreach ($listeners as $listener) {
            $this->eventSubscribers[$event][] = $listener;
            if ($this->onceListeners === null) {
                $this->onceListeners = new WeakMap();
            }
            $this->onceListeners[$listener] = 1 + ($this->onceListeners[$listener] ?? 0);
        }
    }

    /**
     * @inheritdoc
     */
    public function on(int|string|BackedEnum $event, Closure ...$listeners): void {
        $event = $event instanceof BackedEnum ? $event->value : $event;
        foreach ($listeners as $listener) {
            $this->eventSubscribers[$event][] = $listener;
        }
    }

    /**
     * @inheritdoc
     */
    public function off(int|string|BackedEnum $event, Closure ...$handlers): int {
        if ($handlers === []) {
            return $this->clearEventListeners($event);
        }
        $event = $event instanceof BackedEnum ? $event->value : $event;
        $count = 0;

        if (empty($this->eventSubscribers[$event])) {
            return 0;
        }

        foreach ($this->eventSubscribers as $k => $subscriber) {
            if (null !== ($handlerKey = \array_search($subscriber, $handlers))) {
                unset($this->eventSubscribers[$k], $handlers[$handlerKey]);
                ++$count;
            }
        }
        
        return $count;
    }
}