<?php
namespace Gjallarhorn;

use BackedEnum;
use Skuld\Promise;

/**
 * An event emitter class can be used to enable events to be
 * emitted and subscribed to from objects that don't exist yet. 
 * 
 * @package Gjallarhorn
 */
final class EventEmitter implements EventEmitterInterface {
    use EventEmitterTrait {
        emit as _emit;
        emitAsync as _emitAsync;
    }

    public function emit(int|string|BackedEnum|Event $event, mixed ...$args): mixed {
        return self::_emit($event, ...$args);
    }

    public function emitAsync(int|string|BackedEnum|Event $event, mixed ...$args): Promise {
        return self::_emitAsync($event, ...$args);
    }
}