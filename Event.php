<?php
namespace Gjallarhorn;

use ArrayIterator;
use BackedEnum;
use IteratorAggregate;
use Traversable;

/**
 * An event object is passed by reference, and so additional
 * data can be attached to the event object by event listeners.
 * 
 * @package Gjallarhorn
 */
class Event implements IteratorAggregate {

    public int|string|BackedEnum $event;
    public array $parameters;

    public function __construct(int|string|BackedEnum $event, array $parameters) {
        $this->event = $event;
        $this->parameters = $parameters;
    }

    public function &__get(string $name): mixed {
        return $this->parameters[$name] ?? null;
    }

    public function __isset(string $name) {
        return isset($this->parameters[$name]);
    }

    public function getIterator(): Traversable {
        return new ArrayIterator($this->parameters);
    }
}